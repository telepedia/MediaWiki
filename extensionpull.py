import os
import shutil
import subprocess
import sys
import argparse
import yaml

# Define the path to the local YAML file
yaml_path = '/var/www/html/mediawiki1/extensions.yaml'

# Load the YAML content
with open(yaml_path, 'r') as f:
    data = yaml.safe_load(f)

# Create argument parser
parser = argparse.ArgumentParser(description='Clone MediaWiki extensions from YAML configuration.')
parser.add_argument('branch', metavar='branch_name', type=str, help='Branch name to clone extensions from')
parser.add_argument('--overwrite', action='store_true', help='Overwrite an extension if it already exists in /extensions/')
parser.add_argument('--all', action='store_true', help='Clone all extensions')
parser.add_argument('--exclude', type=str, metavar='extension_name', help='Exclude a particular extension from cloning')
parser.add_argument('--extension', type=str, metavar='extension_name', help='Clone only one extension')
args = parser.parse_args()

# Validate branch name against values in the data dictionary
valid_branches = [data[key]['branch'] for key in data]
if args.branch not in valid_branches:
    print("Error: Invalid branch name. Please provide a valid branch name.")
    sys.exit(1)

# Iterate through each extension
for extension_name, extension_data in data.items():
    # Skip if excluded
    if args.exclude and args.exclude == extension_name:
        print(f'Skipping {extension_name} as it is excluded')
        continue

    # Skip if not specified in --extension argument
    if args.extension and args.extension != extension_name:
        continue

    repo_url = extension_data['repo_url']
    path = extension_data['path']
    print(f'Cloning {extension_name} from {repo_url} (branch: {args.branch}) to {path}...')

    # Check if directory already exists
    if os.path.exists(path):
        if args.overwrite:
            print(f'{path} already exists. Removing...')
            shutil.rmtree(path)
        else:
            print(f'{path} already exists. Skipping...')
            continue

    # Run git clone command with specific branch to clone the repository
    subprocess.run(['git', 'clone', '-b', args.branch, repo_url, path], check=True)
    print(f'Successfully cloned {extension_name} (branch: {args.branch}) to {path}')
