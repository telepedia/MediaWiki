# Telepedia Production MediaWiki Configuration

[Telepedia] is a wiki farm that provides free MediaWiki hosting. Telepedia is powered by volunteers only, and is comitted to open-source values. 

This repository contains our production MediaWiki configuration. Contributions (see below) are always welcome.

# Contributing

Pull requests are welcome! Please read [our contributing guide](CONTRIBUTING.md) for more information!

# Issues

Our issue tracker can be found on [Phorge](https://phorge.telepedia.net/maniphest/). You may directly open a new issue by clicking [here](https://phorge.telepedia.net/maniphest/task/edit/form/1/). Please read below for security-related concerns.

# Security Vulnerabilities

If you believe you have found a security vulnerability in any part of our code, please do not post it publicly by using our wikis or bug trackers for that; rather please follow the below instrtuctions.

As a quick overview, you can email security concerns to community@telepedia.net or if you'd like, you can instead directly create a security-related task [here](https://phorge.telepedia.net/maniphest/task/edit/), but please keep the "Security" project attached to the task.
