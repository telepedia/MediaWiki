import requests
import subprocess

def generate_sitemap(url, dbname):
    # Construct the command
    command = f"php /var/www/html/mediawiki1/maintenance/generateSitemap.php --fspath=/var/www/html/mediawiki1/sitemap/ --server={url} --urlpath=/sitemap/ --wiki={dbname}"

    try:
        # Run the command using subprocess
        subprocess.run(command, shell=True, check=True)
        print(f"Sitemap generated for {dbname} successfully.")
    except subprocess.CalledProcessError as e:
        print(f"Error generating sitemap for {dbname}: {e}")

# Call the API
api_url = 'https://meta.telepedia.net/api.php?action=wikidiscover&format=json&wdstate=active%7Cpublic&wdsiteprop=url%7Cdbname'
try:
    response = requests.get(api_url)
    response.raise_for_status()
    data = response.json()

    # Iterate over each item in the array
    for item in data['wikidiscover']:
        url = item['url']
        dbname = item['dbname']
        generate_sitemap(url, dbname)
except requests.exceptions.RequestException as e:
    print(f"Error calling the API: {e}")
except KeyError:
    print("Invalid API response format.")
except ValueError:
    print("Invalid JSON response from the API.")
