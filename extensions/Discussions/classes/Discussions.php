<?php

use MediaWiki\MediaWikiServices;
use Wikimedia\Rdbms\SelectQueryBuilder;
class SpecialDiscussions extends SpecialPage {

    public $limit = 50;

    public $pagerLimit = 9;

    public $currentPagerPage = 0;

    public $allow = '';

    public $voting = '';

    public $allowPlus = true;

    public $allowMinus = true;
    public $comments = [];

    public function __construct()
    {
        parent::__construct('Discussions');
    }

    public function execute($par)
    {
        $request = $this->getRequest();
        $user = $this->getUser();

        // Check that the user is allowed to access this special page...
        if (!$user->isAllowed('discussions')) {
            throw new PermissionsError('discussions');
        }

        $this->setHeaders();
        $this->outputHeader();
        $this->getOutput()->addModules('ext.discussions');
        $this->displayForm();
    }

    public function setVoting( $voting ) {
        $this->voting = $voting;
        $voting = strtoupper( $voting );

        if ( $voting == 'OFF' ) {
            $this->allowMinus = false;
            $this->allowPlus = false;
        }
        if ( $voting == 'PLUS' ) {
            $this->allowMinus = false;
        }
        if ( $voting == 'MINUS' ) {
            $this->allowPlus = false;
        }
    }

    public function countTotal() {
        $lb = MediaWikiServices::getInstance()->getDBLoadBalancer();
        $dbr = $lb->getConnectionRef(DB_REPLICA);
        $count = 0;
        $s = $dbr->newSelectQueryBuilder()
            ->select( 'COUNT(*) AS discussions_count' )
            ->from( 'discussions' )
            ->caller( __METHOD__ )
            ->fetchResultSet();

        if ( $s !== false ) {
            $count = $s->discussions_count;
        }
        return $count;
    }

    public function getLatestDiscussionID() {
        $latestDiscussionID = 0;
        $lb = MediaWikiServices::getInstance()->getDBLoadBalancer();
        $dbr = wfGetDB( DB_REPLICA );
        $s = $dbr->newSelectQueryBuilder()
            ->select( 'discussions_id' )
            ->from( 'discussions' )
            ->caller( __METHOD__ )
            ->orderBy( 'discussions_date, SelectQueryBuilder::Sort_ASC')
            ->limit( 1 )
            ->fetchResultSet();

        if ( $s !== false ) {
            $latestDiscussionsID = $s->discussions_id;
        }
            return $latestDiscussionsID;
    }

    /**
     * Fetches all comments, called by display().
     *
     * @return array Array containing every possible bit of information about
     * 				a comment, including score, timestamp and more
     */
    public function getDiscussionThreads() {
        $discussionThreads = [];
        $discussionsData = [];
        $discussions = [];

        // Try to fetch page comments from cache first
        $cache = ObjectCache::getInstance( CACHE_ANYTHING );
        $cacheKey = $cache->makeKey(
            'discussions',
            $this->id,
            $this->getCurrentPagerPage(),
            $this->getSort()
        );
        $cachedValue = $cache->get( $cacheKey );
        if ( !$cachedValue ) {
            // Fetch comments data from database if cache was not hit
            $discussionsData = $this->getDiscussionsDataFromDatabase();
            $cache->set( $cacheKey, $discussionsData );
        } else {
            // Get comments data from cache if it was hit
            $discussionsData = $cachedValue;
        }

        // Build Comment's from data array
        foreach ( $discussionssData as $data ) {
            $discussions[] = new discussions( $this, $this->getContext(), $data );
        }

        // Build top comments array
        foreach ( $discussions as $discussion ) {
            if ( $discussion->parentID == 0 ) {
                $discussionThreads[$discussion->id][] = $discussion;
            }
        }

        // Build threads array
        foreach ( $discussions as $discussion ) {
            if ( $discussion->parentID != 0 ) {
                $discussionThreads[$discussion->parentID][] = $discussion;
            }
        }

        // Sort replies, always descending
        foreach ( $discussionThreads as &$thread ) {
            if ( count( $thread ) ) {
                usort( $thread, [ 'discussionsFunctions', 'sortDescDiscussions' ] );
            }
        }

        return $discussionThreads;
    }


    /**
     * @return int The page we are currently paged to
     * not used for any API calls
     */
    public function getCurrentPagerPage() {
        if ( $this->currentPagerPage == 0 ) {
            $this->currentPagerPage = $this->getRequest()->getInt( $this->pageQuery, 1 );

            if ( $this->currentPagerPage < 1 ) {
                $this->currentPagerPage = 1;
            }
        }

        return $this->currentPagerPage;
    }

    public function displayPager( $pagerCurrent, $pagesCount ) {
        // Middle is used to "center" pages around the current page.
        $pager_middle = ceil( $this->pagerLimit / 2 );
        // first is the first page listed by this pager piece (re quantity)
        $pagerFirst = $pagerCurrent - $pager_middle + 1;
        // last is the last page listed by this pager piece (re quantity)
        $pagerLast = $pagerCurrent + $this->pagerLimit - $pager_middle;

        // Prepare for generation loop.
        $i = $pagerFirst;
        if ( $pagerLast > $pagesCount ) {
            // Adjust "center" if at end of query.
            $i = $i + ( $pagesCount - $pagerLast );
            $pagerLast = $pagesCount;
        }
        if ( $i <= 0 ) {
            // Adjust "center" if at start of query.
            $pagerLast = $pagerLast + ( 1 - $i );
            $i = 1;
        }

        $output = '';
        if ( $pagesCount > 1 ) {
            $output .= '<ul class="c-pager">';
            $pagerEllipsis = '<li class="c-pager-item c-pager-ellipsis"><span>...</span></li>';

            // Whether to display the "Previous page" link
            if ( $pagerCurrent > 1 ) {
                $output .= '<li class="c-pager-item c-pager-previous">' .
                    Html::rawElement(
                        'a',
                        [
                            'rel' => 'nofollow',
                            'class' => 'c-pager-link',
                            'href' => '#cfirst',
                            'data-' . $this->pageQuery => ( $pagerCurrent - 1 ),
                        ],
                        '&lt;'
                    ) .
                    '</li>';
            }

            // Whether to display the "First page" link
            if ( $i > 1 ) {
                $output .= '<li class="c-pager-item c-pager-first">' .
                    Html::rawElement(
                        'a',
                        [
                            'rel' => 'nofollow',
                            'class' => 'c-pager-link',
                            'href' => '#cfirst',
                            'data-' . $this->pageQuery => 1,
                        ],
                        1
                    ) .
                    '</li>';
            }

            // When there is more than one page, create the pager list.
            if ( $i != $pagesCount ) {
                if ( $i > 2 ) {
                    $output .= $pagerEllipsis;
                }

                // Now generate the actual pager piece.
                for ( ; $i <= $pagerLast && $i <= $pagesCount; $i++ ) {
                    if ( $i == $pagerCurrent ) {
                        $output .= '<li class="c-pager-item c-pager-current"><span>' .
                            $i . '</span></li>';
                    } else {
                        $output .= '<li class="c-pager-item">' .
                            Html::rawElement(
                                'a',
                                [
                                    'rel' => 'nofollow',
                                    'class' => 'c-pager-link',
                                    'href' => '#cfirst',
                                    'data-' . $this->pageQuery => $i,
                                ],
                                $i
                            ) .
                            '</li>';
                    }
                }

                if ( $i < $pagesCount ) {
                    $output .= $pagerEllipsis;
                }
            }

            // Whether to display the "Last page" link
            if ( $pagesCount > ( $i - 1 ) ) {
                $output .= '<li class="c-pager-item c-pager-last">' .
                    Html::rawElement(
                        'a',
                        [
                            'rel' => 'nofollow',
                            'class' => 'c-pager-link',
                            'href' => '#cfirst',
                            'data-' . $this->pageQuery => $pagesCount,
                        ],
                        $pagesCount
                    ) .
                    '</li>';
            }

            // Whether to display the "Next page" link
            if ( $pagerCurrent < $pagesCount ) {
                $output .= '<li class="c-pager-item c-pager-next">' .
                    Html::rawElement(
                        'a',
                        [
                            'rel' => 'nofollow',
                            'class' => 'c-pager-link',
                            'href' => '#cfirst',
                            'data-' . $this->pageQuery => ( $pagerCurrent + 1 ),
                        ],
                        '&gt;'
                    ) .
                    '</li>';
            }

            $output .= '</ul>';
        }

        return $output;
    }

    /**
     * Get this list of anon commenters in the given list of comments,
     * and return a mapped array of IP adressess to the number anon poster
     * (so anon posters can be called Anon#1, Anon#2, etc
     *
     * @return array
     */
    public function getAnonList() {
        $counter = 1;
        $bucket = [];

        $discussionsThreads = $this->discussions;

        $discussions = []; // convert 2nd threads array to a simple list of comments
        foreach ( $discussionsThreads as $thread ) {
            $discussions = array_merge( $discussions, $thread );
        }
        usort( $discussions, [ 'discussionsFunctions', 'sortTime' ] );

        foreach ( $discussions as $discussion ) {
            if (
                !array_key_exists( $discussion->user->getName(), $bucket ) &&
                $discussion->user->isAnon()
            ) {
                $bucket[$discussion->username] = $counter;
                $counter++;
            }
        }

        return $bucket;
    }

    /**
     * Display all the comments for the current page.
     * CSS and JS is loaded in CommentsHooks.php
     * @return string
     */
    public function display() {
        $output = '';

        $discussionThreads = $this->getDiscussionThreads();
        $this->$discussions = $discussionThreads;

        $currentPageNum = $this->getCurrentPagerPage();
        $numPages = (int)ceil( $this->countTotal() / $this->limit );

        // Load complete blocked list for logged in user so they don't see their comments
        $blockList = [];
        if ( $this->getUser()->isRegistered() ) {
            $blockList = discussionFunctions::getBlockList( $this->getUser() );
        }

        $pager = $this->displayPager( $currentPageNum, $numPages );
        $output .= $pager;
        $output .= '<a id="cfirst" name="cfirst" rel="nofollow"></a>';

        $anonList = $this->getAnonList();

        foreach ( $discussionThreads as $thread ) {
            foreach ( $thread as $discussion ) {
                $output .= $discussion->display( $blockList, $anonList );
            }
        }
        $output .= $pager;

        return $output;
    }

    /**
     * Displays the "Sort by X" form and a link to auto-refresh comments
     *
     * @return string HTML
     */
    public function displayOrderForm() {
        $output = '<div class="c-order">
			<div class="c-order-select">
				<form name="ChangeOrder" action="">
					<select name="TheOrder">
						<option value="0">' .
            wfMessage( 'discussions-sort-by-date' )->plain() .
            '</option>
						<option value="1">' .
            wfMessage( 'discussions-sort-by-score' )->plain() .
            '</option>
					</select>
				</form>
			</div>
			<div id="spy" class="c-spy">
				<a href="javascript:void(0)">' .
            wfMessage( 'discussions-auto-refresher-enable' )->plain() .
            '</a>
			</div>
			<div class="visualClear"></div>
		</div>
		<br />' . "\n";

        return $output;
    }

    public function displayForm() {
        $output = '<form action="" method="post" name="commentForm">' . "\n";

        $pos = false;
        if ( $this->allow ) {
            $pos = strpos(
                strtoupper( addslashes( $this->allow ) ),
                strtoupper( addslashes( $this->getUser()->getName() ) )
            );
        }

        $user = $this->getUser();

        // Use these for the block/global block check below
        $context = RequestContext::getMain();
        $userContext = $context->getUser();
        $language = $context->getLanguage();
        $ip = $context->getRequest()->getIP();

        // Check users block status
        if ( $user->getBlock() ) {
            $output .= MediaWikiServices::getInstance()->getBlockErrorFormatter()
                ->getMessage( $user->getBlock(), $userContext, $language, $ip )
                ->parse();
        } elseif ( $user->isBlockedGlobally() ) {
            $output .= MediaWikiServices::getInstance()->getBlockErrorFormatter()
                ->getMessage( $user->getGlobalBlock(), $userContext, $language, $ip )
                ->parse();
        } elseif ( !$this->getUser()->isAllowed( 'discussions' ) ) {
            // 'comment' user right is required to add new comments
            $output .= wfMessage( 'discussions-not-allowed' )->parse();
            $output .= '<input type="hidden" name="lastCommentId" value="' . $this->getLatestDiscussionsID() . '" />' . "\n";
        } else {
            $output .= '<div class="c-form-title">' . wfMessage( 'discussions-submit' )->plain() . '</div>' . "\n";
            $output .= '<div id="replyto" class="c-form-reply-to"></div>' . "\n";
            // Show a message to anons, prompting them to register or log in
            if ( !$user->isRegistered() ) {
                $output .= '<div class="c-form-message">' .
                    wfMessage( 'discussions-anon-message' )->parse() . '</div>' . "\n";
            }

            $output .= '<textarea name="commentText" id="discussion" rows="5" cols="64"></textarea>' . "\n";
            $output .= '<div class="discussions-preview"></div>';
            $output .= '<div class="c-form-button">';
            $output .= '<input type="button" value="' . wfMessage( 'discussions-post' )->escaped() .
                '" class="site-button" name="wpSubmitComment" />' . "\n";
            $output .= '<input type="button" value="' . wfMessage( 'showpreview' )->escaped() .
                '" class="site-button" name="wpPreview" />';
            $output .= '</div>' . "\n";
        }
        $output .= '<input type="hidden" name="action" value="purge" />' . "\n";
        $output .= '<input type="hidden" name="pageId" value="' . $this->id . '" />' . "\n";
        $output .= '<input type="hidden" name="discussionid" />' . "\n";
        $output .= '<input type="hidden" name="lastDiscussionId" value="' . $this->getLatestDiscussionID() . '" />' . "\n";
        $output .= '<input type="hidden" name="discussionParentId" />' . "\n";
        $output .= '<input type="hidden" name="' . $this->pageQuery . '" value="' . $this->getCurrentPagerPage() . '" />' . "\n";
        $output .= Html::hidden( 'token', $this->getUser()->getEditToken() );
        $output .= '</form>' . "\n";
        return $output;
    }

    /**
     * Purge caches (parser cache and Squid cache)
     */
    public function clearDiscussionsListCache() {
        wfDebug( "Clearing discussions for page {$this->id} from cache\n" );
        $cache = ObjectCache::getInstance( CACHE_ANYTHING );
        // Delete all possible keys for the page
        // @TODO: this duplicates values returned by getSort()
        $sorts = [ 'discussions_score DESC', 'timestamp DESC', 'timestamp ASC' ];
        $pages = (int)ceil( $this->countTotal() / $this->limit );
        $keys = [];
        foreach ( $sorts as $sort ) {
            for ( $page = 0; $page <= $pages; $page++ ) {
                $keys[] = $cache->makeKey(
                    'discussions',
                    $this->id,
                    $page,
                    $sort
                );
            }
        }
        $cache->deleteMulti( $keys );
    }

    /**
     * Builds sort part of query depending on options set
     * @return string Sort ordering to use as the ORDER BY statement in an SQL query
     */
    private function getSort() {
        global $wgDiscussionsSortDescending;

        if ( $this->orderBy ) {
            return 'discussions_score DESC';
        } elseif ( $wgDiscussionsSortDescending ) {
            return 'timestamp DESC';
        } else {
            return 'timestamp ASC';
        }
    }

    /**
     * Fetches comments data from database
     * @return array
     */
    private function getDiscussionsDataFromDatabase() {
        $lb = MediaWikiServices::getInstance()->getDBLoadBalancer();
        $dbr = $lb->getConnectionRef(DB_REPLICA);
        // Defaults (for non-social wikis)
        $tables = [
            'discussion',
            'vote1' => 'discussions_Vote',
            'vote2' => 'discussion_Vote',
        ];
        $fields = [
            'discussion_IP', 'discussion_Text', 'discussion_actor',
            'discussion_Date', 'discussion_Date AS timestamp',
            'discussionID', 'discussion_Parent_ID',
            // @todo FIXME: this and the stats_total_points are buggy on PostgreSQL
            // Skizzerz says that the whole query is bugged in general but MySQL "helpfully"
            // ignores the bugginess and returns potentially incorrect results
            // I just lazily slapped current_vote (and stats_total_points in the "if SP is installed" loop)
            // to the GROUP BY condition to try to remedy this. --ashley, 17 January 2020
            'vote1.discussion_Vote_Score AS current_vote',
            'SUM(vote2.discussion_Vote_Score) AS discussion_score'
        ];
        $joinConds = [
            // For current user's vote
            'vote1' => [
                'LEFT JOIN',
                [
                    'vote1.discussion_Vote_ID = discussionID',
                    'vote1.discussion_Vote_actor' => $this->getUser()->getActorId()
                ]
            ],
            // For total vote count
            'vote2' => [ 'LEFT JOIN', 'vote2.discussion_Vote_ID = discussionID' ]
        ];
        $params = [ 'GROUP BY' => 'discussionID, current_vote' ];

        // If SocialProfile is installed, query the user_stats table too.
        if (
            class_exists( 'UserProfile' ) &&
            $dbr->tableExists( 'user_stats' )
        ) {
            $tables[] = 'user_stats';
            $fields[] = 'stats_total_points';
            $joinConds['discussions'] = [
                'LEFT JOIN', 'discussion_actor = stats_actor'
            ];
            $params['GROUP BY'] .= ', stats_total_points';
        }

        // Sort
        $params['ORDER BY'] = $this->getSort();

        // Limit amount of comments being queried based on
        // per page limit and current page offset
        $params['LIMIT'] = $this->limit;
        $params['OFFSET'] = ( $this->currentPagerPage - 1 ) * $this->limit;

        // Perform the query
        $res = $dbr->select(
            $tables,
            $fields,
            [ 'discussion_id' => $this->id ],
            __METHOD__,
            $params,
            $joinConds
        );

        $discussionsData = [];

        foreach ( $res as $row ) {
            if ( $row->discussion_Parent_ID == 0 ) {
                $thread = $row->discussionID;
            } else {
                $thread = $row->discussion_Parent_ID;
            }
            $data = [
                'discussion_actor' => $row->discussion_actor,
                'discussion_IP' => $row->discussion_IP,
                'discussion_Text' => $row->discussion_Text,
                'discussion_Date' => $row->discussion_Date,
                'discussion_user_points' => ( isset( $row->stats_total_points ) ? number_format( $row->stats_total_points ) : 0 ),
                'discussionID' => $row->discussionID,
                'discussion_Parent_ID' => $row->discussion_Parent_ID,
                'thread' => $thread,
                'timestamp' => wfTimestamp( TS_UNIX, $row->timestamp ),
                'current_vote' => ( isset( $row->discussion_vote ) ? $row->discussion_vote : false ),
                'total_vote' => ( isset( $row->discussion_score ) ? $row->discussion_score : 0 ),
            ];

            $discussionsData[] = $data;
        }

        return $discussionsData;
    }

}
