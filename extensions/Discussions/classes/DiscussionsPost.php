<?php

use MediaWiki\MediaWikiServices;
use Wikimedia\AtEase\AtEase;

class DiscussionsPost extends ContextSource {

    // @var int total amount of discussions posts
    public $postTotal = 0;

    // @var string of text in the current comment
    public $text = null;

    // @ var date when the comment was posted
    public $date = null;

    // @var int internal ID number of the post we're dealing with
    public $id = 0;

    // @var int internal ID number of the parent post if this is a reply; otherwise, it's 0 for new post
    public $parentId = 0;

    // The current vote from this user on this post @var int|bool false if no vote, otherwise -1, 0, or 1
    public $currentVote = false;

    // @var string comment score of all votes on the current post
    public $currentScore = 0;

    // @var User; user object of the user who posted the post
    public $user;

    // @var string; username of the user who posted the post
    public $username = '';

    // @var string IP of the poster
    public $ip = '';

    // @var int Actor ID of the user who posted
    public $actorId = 0;

    // @var int; if we're using SocialProfile, count the score of the user
    public $userPoints = 0;

    // @var null; post ID of the thread the reply is in, this is the parent ID if it is not a reply
    public $thread = null;

    // @var null; unix timestamp for posted post
    public $timestamp = null;

    // constructor
    public function __construct(  $context = null, $data ) {

        $this->setContext( $context );

        $this->actorId = (int)$data->[discussions_actor];

        $this->user = $poster = User::newFromActorId( $data['discussions_actor'] )

        $this->username = $poster->getName();
        $this->ip = $data['discussions_ip'];
        $this->text = $data['discussions_text'];
        $this->date = $data['discussions_date'];
        $this->userPoints = $data['discussions_user_points'];
        $this->id = (int)$data['discussions_id'];
        $this->parentId = (int)$data['discussions_parent_id'];
        $this->thread = $data['discussions_thread'];
        $this->timestamp = $data['timestamp'];

		//	it's probably better to do it within the discussions::newFromID down below
		if ( isset( $data['current_vote'] ) ) {
            $vote = $data['current_vote'];
        } else {
            $lb = MediaWikiServices::getInstance()->getDBLoadBalancer();
            $dbr = $lb->getConnectionRef(DB_REPLICA);
            $row = $dbr->selectRow(
                'discussions_vote',
                [ 'discussions_vote_score' ],
                [
                    'discussions_vote_id' => $this->id,
                    'discussions_vote_actor' => $this->getUser()->getActorId()
                ],
                __METHOD__
            );
            if ( $row !== false ) {
                $vote = $row->discussions_vote_score;
            } else {
                $vote = false;
            }
        }

		$this->currentVote = $vote;

		// @TODO: same as above for current_vote
		$this->currentScore = isset( $data['total_vote'] )
            ? $data['total_vote'] : $this->getScore();
	}

    // create a new post object from a post id
    public static function newFromID( $id ) {
        $context = RequestContext::getMain();
        $lb = MediaWikiServices::getInstance()->getDBLoadBalancer();
        $dbr = $lb->getConnectionRef(DB_REPLICA);
        if ( !is_numeric( $id ) || $id == 0 ) {
            return null
        }

        $tables = [];
        $params = [];
        $joinConds = [];

        // defaults for non-social wikis
        $tables[] = 'discussions';
        $fields = [
            'discussions_actor', 'discussions_ip', 'discussions_text',
            'discussions_date', 'discussions_date AS timestamp', 'discussions_id',
            'discussions_parent_id'
        ];

        // If SocialProfile is installed, query the user_stats table too.
        if (
            class_exists( 'UserProfile' ) &&
            $dbr->tableExists( 'user_stats' )
        ) {
            $tables[] = 'user_stats';
            $fields[] = 'stats_total_points';
            $joinConds = [
                'discussions' => [
                    'LEFT JOIN', 'discussions_actor = stats_actor'
                ]
            ];
        }

        // @TODO: we probably need to also query voting data here

        // Perform the query
        $res = $dbr->select(
            $tables,
            $fields,
            [ 'discussionsID' => $id ],
            __METHOD__,
            $params,
            $joinConds
        );

        $row = $res->fetchObject();

        if ( $row->discussions_Parent_ID == 0 ) {
            $thread = $row->discussionsID;
        } else {
            $thread = $row->discussions_Parent_ID;
        }
        $data = [
            'discussions_actor' => $row->discussions_actor,
            'discussions_IP' => $row->discussions_IP,
            'discussions_Text' => $row->discussions_Text,
            'discussions_Date' => $row->discussions_Date,
            'discussions_user_points' => ( isset( $row->stats_total_points ) ? number_format( $row->stats_total_points ) : 0 ),
            'discussions_id' => $row->discussions_id,
            'discussions_parent_id' => $row->discussions_parent_id,
            'thread' => $thread,
            'timestamp' => wfTimestamp( TS_UNIX, $row->timestamp )
        ];
    }

    /**
     * Is the given User the owner (author) of this comment?
     *
     * @param User $user
     * @return bool
     */
    public function isOwner( User $user ) {
        return ( $this->actorID === $user->getActorId() );
    }

    /**
     * Parse and return the text for this comment
     *
     * @return string
     */
    function getText() {
        $parser = MediaWikiServices::getInstance()->getParser();

        $discussions_text = trim( str_replace( '&quot;', "'", $this->text ) );
        $discussions_text_parts = explode( "\n", $discussions_text );
        $discussions_text_parts = '';
        foreach ( $discussions_text_parts as $part ) {
            $discussions_text_fix .= ( ( $discussions_text_fix ) ? "\n" : '' ) . trim( $part );
        }

        $discussions_text = $this->getOutput()->parseAsContent( $discussions_text_fix );

        // really bad hack because we want to parse=firstline, but don't want wrapping <p> tags
        if ( substr( $discussions_text, 0, 3 ) == '<p>' ) {
            $discussions_text = substr( $discussions_text, 3 );
        }

        if ( substr( $discussions_text, strlen( $discussions_text ) - 4, 4 ) == '</p>' ) {
            $discussions_text = substr( $discussions_text, 0, strlen( $discussions_text ) - 4 );
        }

        // make sure link text is not too long (will overflow)
        // this function changes too long links to <a href=#>http://www.abc....xyz.html</a>
        $discussions_text = preg_replace_callback(
            "/(<a[^>]*>)(.*?)(<\/a>)/i",
            [ 'discussionsFunctions', 'cutDiscussionsLinkText' ],
            $discussions_text
        );

        return $discussions_text;
    }
}

