<?php

use MediaWiki\MediaWikiServices;
class TelepediaAds
{
    // can we show ads on the current page?
    public static $PAGE_HAS_ADS = false;

    private static function canShowAds( User $user ) {
        global $wgAdConfig, $wgTitle, $wgRequest;
        if ( !$wgAdConfig['enabled'] ) {
            return false;
        }

        $effectiveGroups = MediaWikiServices::getInstance()->getUserGroupManager()->getUserEffectiveGroups( $user );
        if (
            $wgTitle instanceof Title && $wgTitle->isSpecial( 'Userlogin' ) ||
            in_array( 'user', $effectiveGroups ) && !$wgRequest->getVal( 'forceads' )
        ) {
            return false;
        }

        // No configuration for this skin? Bail out!
        if ( !isset( $wgAdConfig[self::determineSkin()] ) ) {
            return false;
        }

        return true;
    }

    /**
     * Check if the current namespace is allowed to show ads.
     *
     * @return bool True if the namespace is supported, otherwise false
     */
    public static function isEnabledNamespace() {
        global $wgAdConfig;

        $title = RequestContext::getMain()->getTitle(); // @todo FIXME filthy hack
        if ( !$title ) {
            return false;
        }
        $namespace = $title->getNamespace();

        if ( in_array( $namespace, $wgAdConfig['namespaces'] ) ) {
            return true;
        } else {
            return false;
        }
    }

    public static function getLeaderboardHTML() {
        global $wgAdConfig;

        $skinName = self::determineSkin();

        $adSlot = '';
        if ( isset( $wgAdConfig[$skinName . '-leaderboard-ad-slot'] ) ) {
            $adSlot = $wgAdConfig[$skinName . '-leaderboard-ad-slot'];
        }

        if ( isset( $wgAdConfig['debug'] ) && $wgAdConfig['debug'] === true ) {
            return '<!-- Begin leaderboard ad (TelepediaAds) -->
			<!-- Ezoic - under_page_title_main_page - under_page_title -->
			<div id="ezoic-pub-ad-placeholder-101">
		<div id="' . $skinName . '-leaderboard-ad" class="' . $skinName . '-ad noprint">
			<img src="http://www.google.com/help/hc/images/adsense_185665_adformat-text_728x90.png" alt="" />
		</div>
<!-- End leaderboard ad (TelepediaAds/Ezoic) --></div><!-- End Ezoic - under_page_title_main_page - under_page_title -->' . "\n";
        }

        if ( isset( $wgAdConfig['mode'] ) && $wgAdConfig['mode'] == 'responsive' ) {
            $adHTML = '<ins class="adsbygoogle"
				style="display:block"
				data-ad-client="ca-pub-' . $wgAdConfig['adsense-client'] . '"
				data-ad-slot="' . $adSlot . '"
				data-ad-format="auto"></ins>
			<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>';
        } else {
            $borderColorMsg = wfMessage( 'shoutwiki-' . $skinName . '-leaderboard-ad-color-border' )->inContentLanguage();
            $colorBGMsg = wfMessage( 'shoutwiki-' . $skinName . '-leaderboard-ad-color-bg' )->inContentLanguage();
            $colorLinkMsg = wfMessage( 'shoutwiki-' . $skinName . '-leaderboard-ad-color-link' )->inContentLanguage();
            $colorTextMsg = wfMessage( 'shoutwiki-' . $skinName . '-leaderboard-ad-color-text' )->inContentLanguage();
            $colorURLMsg = wfMessage( 'shoutwiki-' . $skinName . '-leaderboard-ad-color-url' )->inContentLanguage();

            $colorBorderDefault = 'F6F4C4';
            $colorBGDefault = 'FFFFE0';
            $colorLinkDefault = '000000';
            $colorURLDefault = '002BB8';

            // different defaults for Truglass from old Truglass ad code
            if ( $skinName == 'truglass' ) {
                $colorBorderDefault = 'CDCDCD';
                $colorBGDefault = 'FFFFFF';
                $colorLinkDefault = '0066FF';
                $colorURLDefault = '00A000';
            }

            $adHTML = '<script type="text/javascript"><!--
google_ad_client = "pub-' . $wgAdConfig['adsense-client'] . '";
google_ad_slot = "' . $adSlot . '";
google_ad_width = 728;
google_ad_height = 280;
google_ad_format = "728x280_as";
//google_ad_type = "";
google_ad_channel = "";
google_color_border = ' . Xml::encodeJsVar( $borderColorMsg->isDisabled() ? $colorBorderDefault : $borderColorMsg->text() ) . ';
google_color_bg = ' . Xml::encodeJsVar( $colorBGMsg->isDisabled() ? $colorBGDefault : $colorBGMsg->text() ) . ';
google_color_link = ' . Xml::encodeJsVar( $colorLinkMsg->isDisabled() ? $colorLinkDefault : $colorLinkMsg->text() ) . ';
google_color_text = ' . Xml::encodeJsVar( $colorTextMsg->isDisabled() ? '000000' : $colorTextMsg->text() ) . ';
google_color_url = ' . Xml::encodeJsVar( $colorURLMsg->isDisabled() ? $colorURLDefault : $colorURLMsg->text() ) . ';
//--></script>';
        }
//
//        return ' <!-- Begin leaderboard ad (TelepediaAds) -->
//		<div id="' . $skinName . '-leaderboard-ad" class="' . $skinName . '-ad noprint"><div id="ezoic-pub-ad-placeholder-101">' .
//            $adHTML . '</div></div>
//			<!-- End leaderboard ad (TelepediaAds) --><!-- End Ezoic - under_page_title_main_page - under_page_title -->' . "\n";
                return ' <!-- Begin leaderboard ad (TelepediaAds) -->
                    <!--  [Desktop/Tablet]leaderboard_atf -->
            <div id="top-leaderboard"></div>' . "\n";
    }

    public static function getToolboxHTML() {
        global $wgAdConfig;

        $skinName = self::determineSkin();
        $adSlot = '';

        if ( isset($wgAdConfig[$skinName . '-button-ad-slot'] ) ) {
            $adSlot = $wgAdConfig[$skinName . '-button-ad-slot'];
        }

        return ' <!-- Begin toolbox ad (TelepediaAds) -->
                <!--  [Desktop/Tablet]sky_btf -->
                   <div id="left-sidebar"></div>' . "\n";
    }

    public static function getSkyscraperHTML() {
        global $wgAdConfig;

        $skinName = self::determineSkin();
        $adSlot = '';

        if ( isset($wgAdConfig[$skinName . '-skyscraper-ad-slot'] ) ) {
            $adSlot = $wgAdConfig[$skinName . '-skyscraper-ad-slot'];
        }

        return "\n" . '<!-- Begin skyscraper ads (TelepediaAds) -->
        <div id="column-google" class="' . $skinName . '-ad noprint"><div id="ezoic-pub-ad-placeholder-106"></div>
        </div><!-- End skyscraper ad (TelepediaAds) -->' . "\n";
    }

    public static function getWideSkyHTML() {
        global $wgAdConfig;

        $skinName = self::determineSkin();
        $adSlot = '';

        if ( isset($wgAdConfig[$skinName . '-wide-sky-ad-slot'] ) ) {
            $adSlot = $wgAdConfig[$skinName . '-wide-sky-ad-slot'];
        }

        return "\n" . '<!-- Begin wide skyscraper ad (TelepediaAds) -->
        <div id="column-google" class="' . $skinName . '-ad noprint"></div>
        <!-- End wide skyscraper ad (TelepediaAds) -->' . "\n";
    }

    public static function setupAdCSS( &$out, &$sk ) {
        global $wgAdConfig;

        if ( !$wgAdConfig['enabled'] ) {
            return true;
        }

        // In order for us to load ad-related CSS, the user must either be
        // a mortal (=not staff) or have supplied the forceads parameter in
        // the URL
        $effectiveGroups = MediaWikiServices::getInstance()->getUserGroupManager()
            ->getUserEffectiveGroups( $out->getUser() );
        if (
            !in_array( 'staff', $effectiveGroups ) ||
            $out->getRequest()->getVal( 'forceads' )
        ) {
            $title = $out->getTitle();
            $namespace = $title->getNamespace();

            // Okay, the variable name sucks but anyway...normal page != not login page
            $isNormalPage = $title instanceof Title &&
                !$title->isSpecial( 'Userlogin' );

            // Load ad CSS file when ads are enabled
            if (
                $isNormalPage &&
                in_array( $namespace, $wgAdConfig['namespaces'] )
            ) {
                // Use reflection, some skins are namespaced.
                $skinClass = str_replace( 'Skin', '', ( new ReflectionClass( $sk ) )->getShortName() );
                $skinClass = strtolower( $skinClass );

                if ( isset( $wgAdConfig[$skinClass] ) ) {
                    $modules = [];

                    $resourceLoader = $out->getResourceLoader();

                    // Iterate over the enabled
                    foreach ( $wgAdConfig[$skinClass] as $enabledAdType => $unused ) {
                        $moduleName = "ext.TelepediaAds.{$skinClass}.{$enabledAdType}";

                        // Aurora's sitenotice leaderboard doesn't need any additional CSS
                        if ( $skinClass == 'aurora' && $enabledAdType == 'leaderboard-bottom' ) {
                            continue;
                        }

                        // Special cases have an awful habit of becoming all too common around here...
                        if ( $skinClass == 'refreshed' && isset( $wgAdConfig['mode'] ) && $wgAdConfig['mode'] === 'responsive' ) {
                            $moduleName = 'ext.TelepediaAds.refreshed';
                        }

                        if ( $skinClass == 'monaco' ) {
                            $moduleName = "ext.TelepediaAds.{$skinClass}";
                        }

                        if ( $enabledAdType == 'toolbox' ) {
                            $moduleName = str_replace( 'toolbox', 'button', $moduleName );
                        }

                        // Sanity check -- is there such a module?
                        if ( $resourceLoader->isModuleRegistered( $moduleName ) && $resourceLoader->getModule( $moduleName ) ) {
                            $modules[] = $moduleName;
                        }
                    }

                    if ( !empty( $modules ) ) {
                        $out->addModuleStyles( $modules );
                    }
                }
            }
        }
        return true;
    }

    public static function onSkinAfterPortlet( Skin $skin, $portlet, &$html ) {
        global $wgAdConfig;

        $skins = [ 'vector', 'modern', 'monobook' ];
        $user = $skin->getUser();
        $skin = $skin->getSkinName();

        if (
            in_array( $skin, $skins ) &&
            isset( $wgAdConfig[$skin]['toolbox'] ) &&
            $wgAdConfig[$skin]['toolbox'] &&
            $portlet === 'tb'
        ) {
            $html .= self::loadSidebarAd( 'toolbox-button', $user );
        }
    }

    public static function onMonacoSidebar() {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['monaco']['sidebar'] ) &&
            $wgAdConfig['monaco']['sidebar']
        ) {
            echo self::loadAd( 'sidebar' );
        }
        return true;
    }

    public static function onVectorRightSide($vector, &$adSidebar) {
        global $wgAdConfig, $skinName;
        if (isset($wgAdConfig['vector']['right-side']) && $wgAdConfig['vector']['right-side']) {
            $replacement = '<!-- Begin skyscraper ads (TelepediaAds) -->
            <div id="columnright-google" class="' . $skinName . '-ad noprint"><div id="ezoic-pub-ad-placeholder-106"></div>
            </div><!-- End skyscraper ad (TelepediaAds) -->';

            $adBody = str_replace(
                '<div class="vector-ad"></div>',
                $replacement,
                $adSidebar
            );
            $adSidebar = $adBody;
        }
        return true;
    }


    public static function onMonacoFooter() {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['monaco']['leaderboard'] ) &&
            $wgAdConfig['monaco']['leaderboard']
        ) {
            echo self::loadAd( 'leaderboard' );
        }
        return true;
    }

    public static function onAuroraLeftSidebar( $auroraTemplate ) {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['aurora']['skyscraper'] ) &&
            $wgAdConfig['aurora']['skyscraper']
        ) {
            // Only show this ad on existing pages as it'd strech nonexistent ones
            // quite a bit
            if ( $auroraTemplate->getSkin()->getTitle()->exists() ) {
                // My naming conventions suck, I know.
                echo self::loadAd( 'right-column' );
            }
        }
        return true;
    }

    public static function onTelepediaLeftSidebar( $telepediaTemplate ) {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['telepedia']['skyscraper'] ) &&
            $wgAdConfig['telepedia']['skyscraper']
        ) {
            // Only show this ad on existing pages as it'd strech nonexistent ones
            // quite a bit
            if ( $telepediaTemplate->getSkin()->getTitle()->exists() ) {
                // My naming conventions suck, I know.
                echo self::loadAd( 'skyscraper' );
            }
        }
        return true;
    }

    public static function onSkinAfterContent( &$data, Skin $skin ) {
        global $wgAdConfig;

        $skinName = $skin->getSkinName();
        $user = $skin->getUser();

        if (
            $skinName === 'aurora' &&
            isset( $wgAdConfig['aurora']['leaderboard-bottom'] ) &&
            $wgAdConfig['aurora']['leaderboard-bottom']
        ) {
            $adHTML = self::loadAd( 'leaderboard', $user );
            $data = str_replace(
            // Quick HTML validation fix
                '<div id="aurora-leaderboard-ad"',
                '<div id="aurora-leaderboard-ad-2"',
                $adHTML
            );
        }

        if (
            $skinName === 'vector' &&
            isset( $wgAdConfig['vector']['leaderboard'] ) &&
            $wgAdConfig['vector']['leaderboard']
        ) {
            $data = self::loadAd( 'leaderboard', $user );
        }

        if (
            $skinName === 'timeless' &&
            isset( $wgAdConfig['timeless']['leaderboard'] ) &&
            $wgAdConfig['timeless']['leaderboard']
        ) {
            $data = self::loadAd( 'leaderboard', $user );
        }

        if (
            $skinName === 'telepedia' &&
            isset( $wgAdConfig['telepedia']['skyscraper'] ) &&
            $wgAdConfig['telepedia']['skyscraper']
        ) {
            $data = self::loadAd( 'right-column', $user );
        }

        if (
            $skinName === 'minerva' &&
            isset( $wgAdConfig['minerva']['skyscraper'] ) &&
            $wgAdConfig['minerva']['skyscraper']
        ) {
            $data = self::loadAd( 'right-column', $user );
        }

        if (
            $skinName === 'home' &&
            isset( $wgAdConfig['home']['leaderboard-bottom'] ) &&
            $wgAdConfig['home']['leaderboard-bottom']
        ) {
            $data = self::loadAd( 'leaderboard', $user );
        }

        if (
            $skinName === 'monobook' &&
            isset( $wgAdConfig['monobook']['skyscraper'] ) &&
            $wgAdConfig['monobook']['skyscraper']
        ) {
            $data = self::loadAd( 'right-column', $user );
        }

        return true;
    }

    public static function onDuskAfterToolbox( $dusk ) {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['dusk']['toolbox'] ) &&
            $wgAdConfig['dusk']['toolbox']
        ) {
            echo self::loadAd( 'toolbox-button', $dusk->getSkin()->getUser() );
        }
        return true;
    }

    public static function onGamesSideBox() {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['games']['skyscraper'] ) &&
            $wgAdConfig['games']['skyscraper']
        ) {
            $skyscraperHTML = self::loadAd( 'right-column' );
            if ( empty( $skyscraperHTML ) ) {
                return true;
            }
            $skyscraperHTML = str_replace(
                '<div id="column-google"',
                '<div id="sideads"',
                $skyscraperHTML
            );
            echo $skyscraperHTML;
        }
        return true;
    }

    public static function onHomeAfterEverything( $homeTemplate ) {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['home']['skyscraper'] ) &&
            $wgAdConfig['home']['skyscraper']
        ) {
            $adHTML = self::loadAd( 'skyscraper' );
            if ( $adHTML ) {
                // Remove the IDs altogether
                $adHTML = str_replace(
                    'id="column-google"',
                    '',
                    $adHTML
                );
                // And instead hackily inject the appropriate classes
                $adLeft = str_replace( 'class="', 'class="column-google-left ', $adHTML );
                $adRight = str_replace( 'class="', 'class="column-google-right ', $adHTML );
                $output = $adLeft . $adRight;
                // Finally display 'em, too!
                echo $output;
            }
        }
        return true;
    }

    public static function onMetrolookAfterToolbox( $metrolookTemplate ) {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['metrolook']['toolbox'] ) &&
            $wgAdConfig['metrolook']['toolbox']
        ) {
            echo self::loadAd( 'toolbox-button', $metrolookTemplate->getSkin()->getUser() );
        }
        return true;
    }

    public static function onMetrolookRightPanel( $metrolookTemplate ) {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['metrolook']['wide-skyscraper'] ) &&
            $wgAdConfig['metrolook']['wide-skyscraper']
        ) {
            // Oh gods why...
            $s = self::loadAd( 'wide-skyscraper', $metrolookTemplate->getSkin()->getUser() );
            $s = str_replace(
                [ '<div id="column-google" class="metrolook-ad noprint">', '</div>' ],
                '',
                $s
            );
            echo $s;
        }
        return true;
    }

    public static function onNimbusLeftSide() {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['nimbus']['sidebar'] ) &&
            $wgAdConfig['nimbus']['sidebar']
        ) {
            $ad = self::loadAd( 'small-square' );
            $output = '<div class="bottom-left-nav-container">' . $ad . '</div>';
            echo $output;
        }
        return true;
    }


    public static function onQuartzSidebarWidgetAdvertiser( $quartz, &$adBody ) {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['quartz']['square'] ) &&
            $wgAdConfig['quartz']['square']
        ) {
            $adBody = str_replace(
                'id="quartz-square-ad',
                'id="quartz-square-ad-2',
                self::loadAd( 'square' )
            );
        }
        return true;
    }



    public static function onRefreshedFooter( &$footerExtra ) {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['refreshed']['leaderboard-footer'] ) &&
            $wgAdConfig['refreshed']['leaderboard-footer']
        ) {
            $adHTML = self::loadAd( 'leaderboard' );
            $footerExtra = str_replace(
            // <s>Quick HTML validation fix</s>
            // Just a simple renaming because Refreshed expects the ID to be #advert :|
                '<div id="refreshed-leaderboard-ad" class="refreshed-ad noprint">',
                '<div id="advert" class="refreshed-ad noprint">' .
                // Also inject the title here, as per the MW.org manual page
                wfMessage( 'refreshed-advert' )->parseAsBlock(),
                $adHTML
            );
        }
        return true;
    }

    public static function onRefreshedInSidebar( $tpl ) {
        global $wgAdConfig;
        if (
            isset( $wgAdConfig['refreshed']['sidebar'] ) &&
            $wgAdConfig['refreshed']['sidebar']
        ) {
            // sic!
            // The *slot* is _in_ the sidebar, but what we call a sidebar ad
            // (200x200px) is too wide to be used here!
            echo self::loadAd( 'toolbox-button', $tpl->getSkin()->getUser() );
        }
        return true;
    }
    public static function onSiteNoticeAfter( &$siteNotice, Skin $skin ) {
        global $wgAdConfig;

        $skinName = self::determineSkin();
        $user = $skin->getUser();

        // Monaco and Truglass have a different leaderboard ad implementation
        // and AdSense's terms of use state that one page may have up to three
        // ads; Dusk & DuskToDawn are handled below, as you can see.
        $blacklist = [
            'dusk', 'dusktodawn', 'monaco', 'truglass'
        ];
        if (
            !in_array( $skinName, $blacklist ) &&
            isset( $wgAdConfig[$skinName]['leaderboard'] ) &&
            $wgAdConfig[$skinName]['leaderboard'] === true
        ) {
            $siteNotice .= self::loadAd( 'leaderboard', $user );
        } elseif (
            // Both Dusk* skins have a damn small content area; in fact, it's
            // so small it can't fit a normal leaderboard, so we display a banner
            // in the sitenotice area instead (when enabled in config, of course)
            ( $skinName == 'dusk' || $skinName == 'dusktodawn' ) &&
            isset( $wgAdConfig[$skinName]['banner'] ) &&
            $wgAdConfig[$skinName]['banner'] === true
        ) {
            $siteNotice .= self::loadAd( 'banner', $user );
        }

        return true;
    }


    public static function onBeforePageDisplay( OutputPage $out, Skin $skin ) {

        $title = RequestContext::getMain()->getTitle();

        if ($title == 'Metode Link Alternatif Cmd368 - Situs Judi Slot Agen Bola Online Playtech Slot Login Di Indonesia Gimana Kamu Sanggup Memutuskan Website Paling Baik' ) {
            return;
        } else {

        $out->addHeadItem("ramp", "<script async=\"true\" src='https://www.googletagmanager.com/gtag/js?id=G-DP0QWXMVB3'></script>
                            <script type=\"text/javascript\">
                                window.ramp = window.ramp || {};
                                window.ramp.que = window.ramp.que || [];
                                window.ramp.passiveMode = true;
                            </script>
                            <script>
                                window._pwGA4PageviewId = ''.concat(Date.now());
                                window.dataLayer = window.dataLayer || [];
                                window.gtag = window.gtag || function () {
                                    dataLayer.push(arguments);
                                };
                                gtag('js', new Date());
                                gtag('config', 'G-DP0QWXMVB3', { 'send_page_view': false });
                                gtag(
                                    'event',
                                    'ramp_js',
                                    {
                                        'send_to': 'DP0QWXMVB3',
                                        'pageview_id': window._pwGA4PageviewId
                                    }
                                );
                            </script>
                        <!-- This is the first script -->
                        <script type=\"text/javascript\">
                        window.ramp = window.ramp || {};
                        window.ramp.que = window.ramp.que || [];
                        window.ramp.passiveMode = true;
                        </script>
                        
                        <script type=\"text/javascript\">
                        
                        // Define the ad units that you will use with an array
                        var pwUnits = [
                        {
                        // You can define the selectorId however you want, but the type must match to the ad unit's type
                        selectorId: 'top-leaderboard',
                        type: 'leaderboard_atf'
                        },
                        {
                        type: 'bottom_rail'
                        },
                        {
                        selectorId: 'right-sidebar',
                        type: 'med_rect_btf'
                        },
                        {
                        type: 'flex_leaderboard'
                        },
                        {
                        type: 'trendi_video'
                        },
                        {
                        type: 'desktop_in_article'
                        }
                        ]
                        
                        // Define the init function
                        var init = function () {
                        ramp
                        // pass in the array 'pwUnits' defined right above
                        .addUnits(pwUnits)
                        .then(() => {
                            ramp.displayUnits()
                        }).catch( (e) =>{
                            // catch errors
                            ramp.displayUnits()
                            console.log(e)
                        })
                        }
                        ramp.que.push(init);
                        </script>
                        
                        <!-- This is the second script where you must change the publisher ID and website ID with your own -->
                        <script type=\"text/javascript\"
                        async=\"true\"
                        src=\"//cdn.intergient.com/1025055/74539/ramp.js\">
                        </script>");
                    }
        }

    public static function determineSkin() {
        global $wgOut;

        return strtolower( $wgOut->getSkin()->getSkinName() );
    }

//    public static function onSkinAfterBottomScripts( $skin, &$text ) {
//            $text .= '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>' . "\n";
//        }

    private static function loadAd( $type, $user = null ) {
        if ( $user === null ) {
            // Caller does not have a user
            $user = RequestContext::getMain()->getUser();
        }

        // Early return cases:
        // ** if we can't show ads on the current page (i.e. if it's the login
        // page or something)
        // ** if the wiki's language code isn't supported by Google AdSense
        // ** if ads aren't enabled for the current namespace
        if ( !self::canShowAds( $user ) ) {
            wfDebugLog( 'ShoutWikiAds', 'Early return case #1: can\'t show ads' );
            return '';
        }

        if ( !self::isEnabledNamespace() ) {
            wfDebugLog( 'ShoutWikiAds', 'Early return case #3: ads not enabled for this namespace' );
            return '';
        }

        // Main ad logic starts here
        $allowedAdTypes = [
            'banner', 'leaderboard', 'sidebar', 'toolbox-button',
            'right-column', 'skyscraper', 'small-square', 'square', 'wide-skyscraper'
        ];
        if ( in_array( $type, $allowedAdTypes ) ) {
            self::$PAGE_HAS_ADS = true;
        }

        switch ( $type ) {
            case 'banner':
                return self::getBannerHTML();
            case 'leaderboard':
                return self::getLeaderboardHTML();
            case 'sidebar':
                return self::getSidebarHTML();
            case 'right-column': // old and ugly name
            case 'skyscraper': // standard name used by AdSense documentation, etc.
                return self::getSkyscraperHTML();
            case 'small-square':
                return self::getSmallSquareHTML();
            case 'square':
                return self::getSquareHTML();
            case 'wide-skyscraper':
                return self::getWideSkyscraperHTML();
            default: // invalid type/these ads not enabled in $wgAdConfig
                return '';
        }
    }

    private static function loadSidebarAd( $type, $user = null )
    {
        if ($user === null) {
            // Caller does not have a user
            $user = RequestContext::getMain()->getUser();
        }

        if (!self::isEnabledNamespace()) {
            wfDebugLog('ShoutWikiAds', 'Early return case #3: ads not enabled for this namespace');
            return '';
        }

        $allowedAdTypes = [ 'toolbox-button' ];
        if (in_array( $type, $allowedAdTypes )) {
            self::$PAGE_HAS_ADS = true;
        }

        switch ($type) {
            case 'toolbox-button':
                return self::getToolboxHTML();
            default: // invalid type/these ads not enabled in $wgAdConfig
                return '';
        }
    }
}
