<?php
/**
 * Karma
 * Karma Mediawiki Settings
 *
 * @package   Karma
 * @author    Telepedia
 * @copyright (c) 2023 Telepedia LTD.
 * @license   MIT
 * @link      https://gitlab.com/telepedia/extensions/karma
 **/

if (function_exists('wfLoadExtension')) {
    wfLoadExtension('karma');
    // Keep i18n globals so mergeMessageFileList.php doesn't break
    $wgMessagesDirs['Karma'] = __DIR__ . '/i18n';
    wfWarn(
        'Deprecated PHP entry point used for Karma extension. Please use wfLoadExtension instead, ' .
        'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
    );
    return;
} else {
    die('This version of the Karma extension requires MediaWiki 1.39+');
}
