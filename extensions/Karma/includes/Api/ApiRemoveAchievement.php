<?php

use ApiBase;
use KarmaService;

class ApiRemoveAchievement extends ApiBase
{
    public function execute()
    {
        // You would need to check if the user has the necessary permissions to perform this action.
        $user = $this->getUser();
        if (!$user->isAllowed('achievement-remove')) {
            $this->dieWithError('You do not have permission to remove achievements', 'permission_denied');
        }

        // Get the parameters.
        $params = $this->extractRequestParams();

        // Make sure the required parameters are present.
        if (!isset($params['userId']) || !isset($params['achievementId'])) {
            $this->dieWithError('The userId and achievementId parameters are required', 'missing_params');
        }

        // Do the removal operation
        $karmaService = new KarmaService();
        $karmaService->removeAchievement($params['userId'], $params['achievementId']);

        // Output success message.
        $this->getResult()->addValue(null, $this->getModuleName(), ['status' => 'success']);
    }

    // Describe the parameters
    public function getAllowedParams()
    {
        return [
            'userId' => [
                ApiBase::PARAM_TYPE => 'integer',
                ApiBase::PARAM_REQUIRED => true,
            ],
            'achievementId' => [
                ApiBase::PARAM_TYPE => 'integer',
                ApiBase::PARAM_REQUIRED => true,
            ],
        ];
    }

    // Describe the module
    public function getHelpUrls()
    {
        return 'https://www.mediawiki.org/wiki/Extension:Karma';
    }

    public function mustBePosted()
    {
        // This API module changes data, so it must be posted.
        return true;
    }

    public function isWriteMode()
    {
        // This API module changes data, so it is a write mode.
        return true;
    }
}
