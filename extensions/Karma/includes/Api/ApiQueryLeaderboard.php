<?php

class ApiQueryLeaderboard extends ApiBase {
    public function execute() {
        // Get the leaderboard data from the service.
        $karmaService = new KarmaService();
        $leaderboardData = $karmaService->getLeaderboardData();

        // Replace user_id with username.
        foreach ($leaderboardData as $index => $data) {
            $user = User::newFromId($data['userId']);
            $leaderboardData[$index]['username'] = $user->getName();
            unset($leaderboardData[$index]['user_id']);
        }

        // Add the data to the API result.
        $this->getResult()->addValue( null, $this->getModuleName(), $leaderboardData );
    }

    public function getHelpUrls() {
        return 'https://www.mediawiki.org/wiki/Extension:Karma';
    }

    // No parameters are needed, so we don't need to override getAllowedParams().
}
