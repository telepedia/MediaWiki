<?php

class ApiQueryUserAchievements extends ApiBase {
    public function execute() {
        $params = $this->extractRequestParams();

        if (!isset($params['userId'])) {
            $this->dieWithError('The userId parameter is required', 'missing_params');
        }

        $karmaService = new KarmaService();
        $achievements = $karmaService->getEarnedAchievements($params['userId']);

        $this->getResult()->addValue(null, $this->getModuleName(), $achievements);
    }

    public function getAllowedParams() {
        return [
            'userId' => [
                ApiBase::PARAM_TYPE => 'integer',
                ApiBase::PARAM_REQUIRED => true,
            ],
        ];
    }

    public function getHelpUrls() {
        return 'https://www.mediawiki.org/wiki/Extension:Karma';
    }
}
