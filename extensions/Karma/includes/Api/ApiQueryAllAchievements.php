<?php

/**
 * @todo — make this functional, we don't have getAllAchievements() yet.
 *
 **/

class ApiQueryAllAchievements extends ApiBase {
    public function execute() {
        $karmaService = new KarmaService();
        $achievements = $karmaService->getAllAchievements();

        $this->getResult()->addValue(null, $this->getModuleName(), $achievements);
    }

    public function getHelpUrls() {
        return 'https://www.mediawiki.org/wiki/Extension:Karma';
    }
}
