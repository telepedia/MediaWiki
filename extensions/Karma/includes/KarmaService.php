<?php

use MediaWiki\MediaWikiServices;
use MediaWiki\Permissions\PermissionManager;
use MediaWiki\FileBackend\FileRepo;
use MediaWiki\Permissions\PermissionStatus;
use MediaWiki\Utilities\TimestampException;
use MediaWiki\Revision\RevisionRecord;
use MediaWiki\Revision\SlotRecord;
use MediaWiki\Revision\SuppressedDataException;
use MediaWiki\SpecialPage\SpecialPageFactory;
use MediaWiki\Storage\NameTableAccessException;
use MediaWiki\User\UserOptionsLookup;
use MediaWiki\User\UserIdentityValue;
use MediaWiki\Linker\LinkRenderer;
use Wikimedia\Rdbms\ILoadBalancer;
use Wikimedia\Rdbms\IResultWrapper;
use Wikimedia\Timestamp\ConvertibleTimestamp;
use MediaWiki\Permissions\Authority;
use Wikimedia\Rdbms\DBError;
use MediaWiki\Shell\Shell;
use UploadBase;

class KarmaService {

    public function getAchievementsFromDb() {
        $dbr = wfGetDB(DB_REPLICA);

        $res = $dbr->select(
            'mw_achievements',
            '*',
            [],
            __METHOD__
        );

        $achievements = [];
        foreach ($res as $row) {
            $achievements[] = [
                'id' => $row->id,
                'title' => $row->title,
                'description' => $row->description,
                'image' => $row->image,
                'points' => $row->points,
                'type' => $row->type,
                'progress_required' => $row->progress_required,
            ];
        }

        return $achievements;
    }

    public function getEarnedAchievements($userId) {
        $dbr = wfGetDB(DB_REPLICA);
        $allAchievements = $this->getAchievementsFromDb();

        $res = $dbr->select(
            'mw_user_karma',
            '*',
            [
                'user_id' => $userId,
                'is_removed' => 0   // Only select achievements that have not been removed
            ],
            __METHOD__
        );

        $earnedAchievements = [];
        foreach ($res as $row) {
            $achievement = $allAchievements[array_search($row->achievement_id, array_column($allAchievements, 'id'))];
            $earnedAchievements[] = [
                'achievement_id' => $achievement['id'],
                'description' => $achievement['description'],
                'points_earned' => $row->points_earned,
                'earned_at' => (new Language())->getHumanTimestamp(new MWTimestamp($row->earned_at)),
                'image' => $achievement['image'],
                'title' => $achievement['title']
            ];
        }

        return $earnedAchievements;
    }

    public function getUnearnedAchievements($userId) {
        $dbr = wfGetDB(DB_REPLICA);
        $allAchievements = $this->getAchievementsFromDb();

        $earnedAchievements = $dbr->selectFieldValues(
            'mw_user_karma',
            'achievement_id',
            ['user_id' => $userId],
            __METHOD__
        );

        $unearnedAchievements = [];
        foreach ($allAchievements as $achievement) {
            // Skip this achievement if it's already earned by the user
            if (in_array($achievement['id'], $earnedAchievements)) {
                continue;
            }

            $unearnedAchievements[] = $achievement;
        }

        return $unearnedAchievements;
    }

    public function getTotalPoints($userId) {
        $dbr = wfGetDB(DB_REPLICA);

        $res = $dbr->select(
            'mw_user_karma', // From table
            'SUM(points_earned) AS total_points', // Select total points
            [ 'user_id' => $userId ], // Conditions
            __METHOD__
        );

        if ($row = $res->fetchObject()) {
            return intval($row->total_points);
        }

        return 0;
    }

    /**
     * Protection from gaming the system, Telepedia Staff can remove achievements from users
     * We need to leave an audit trail, so we'll just set the is_removed as true in the database
     * This allows us to restore the achievement if we need to.
     * @param $userId
     * @param $achievementId
     */
    public function removeAchievement($userId, $achievementId) {
        $dbw = wfGetDB(DB_MASTER);

        $dbw->update(
            'mw_user_karma',
            [ 'is_removed' => 1 ],
            [ 'user_id' => $userId, 'achievement_id' => $achievementId ],
            __METHOD__
        );

        // Clear the leaderboard cache after removal
        $cache = ObjectCache::getInstance(CACHE_ANYTHING);
        $cacheKey = $cache->makeKey( 'karma', 'leaderboard' );
        $cache->delete( $cacheKey );

        wfDebugLog( 'Karma', 'Cache for leaderboard was cleared after an achievement was removed.' );
    }

    public function getLeaderboardData($limit = 10)
    {
        // Try to get the leaderboard data from cache first
        $cache = ObjectCache::getInstance(CACHE_ANYTHING);
        $cacheKey = $cache->makeKey( 'karma', 'leaderboard' );
        $leaderboardData = $cache->get( $cacheKey );

        if ( $leaderboardData === false ) {
            // If not found in cache, fetch from DB and then save in cache
            wfDebugLog( 'Karma', 'Leaderboard data not found in cache, fetching from DB.' );

            $dbr = wfGetDB(DB_REPLICA);

            $res = $dbr->select(
                'mw_user_karma',
                [
                    'user_id',
                    'SUM(points_earned) AS total_points',
                ],
                [],
                __METHOD__,
                [
                    'GROUP BY' => 'user_id',
                    'ORDER BY' => 'total_points DESC',
                    'LIMIT' => $limit,
                ]
            );

            $leaderboardData = [];
            foreach ($res as $row) {
                $leaderboardData[] = [
                    'userId' => $row->user_id,
                    'totalPoints' => $row->total_points,
                ];
            }

            // Save the leaderboard data in cache for one hour
            $cache->set( $cacheKey, $leaderboardData, 3600 );

            wfDebugLog( 'Karma', 'Leaderboard data fetched from DB and saved to cache.' );
        } else {
            wfDebugLog( 'Karma', 'Leaderboard data retrieved from cache.' );
        }

        return $leaderboardData;
    }
}
