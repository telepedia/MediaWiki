<?php

class KarmaAchievements {
    const ACHIEVEMENTS = [
        [
            'id' => 1,
            'title' => 'You\'re getting the hang of it!',
            'description' => 'Edit 1 page',
            'criteria' => 2,
            'karma_points' => 100,
            'image' => 'extensions/Karma/images/1 Edit.webp',
        ],
        [
            'id' => 2,
            'title' => 'You\'re a regular editor now!',
            'description' => 'Edit 3 pages',
            'criteria' => 3,
            'karma_points' => 100,
            'image' => 'extensions/Karma/images/10 Edit.webp',
        ],
        [
            'id' => 3,
            'title' => 'You\'re a real contributor now!',
            'description' => 'Edit 4 pages',
            'criteria' => 4,
            'karma_points' => 100,
            'image' => 'extensions/Karma/images/100 Edit.webp',
        ],
        [
            'id' => 4,
            'title' => 'Keep up the good work!',
            'description' => 'Edit 500 pages',
            'criteria' => 7,
            'karma_points' => 100,
            'image' => 'extensions/Karma/images/500 Edit.webp',
        ],
        [
            'id' => 4,
            'title' => 'You\'ll go down in history (literally)!',
            'description' => 'Edit 750 pages',
            'criteria' => 750,
            'karma_points' => 100,
            'image' => 'extensions/Karma/images/750 Edit.webp',
        ],
        [
            'id' => 5,
            'title' => 'You\'re a true historian!',
            'description' => 'Edit 1000 pages',
            'criteria' => 12,
            'karma_points' => 100,
        ],
        // images
        [
            'id' => 6,
            'title' => 'I made this!',
            'description' => 'Upload 10 images',
            'criteria' => 10,
            'karma_points' => 125,
            'image' => 'extensions/Karma/images/Upload 10.webp',
        ],
        [
            'id' => 7,
            'title' => 'Art gatherer',
            'description' => 'Upload 100 images',
            'criteria' => 50,
            'karma_points' => 125,
            'image' => 'extensions/Karma/images/Upload 100.webp',
        ],
        [
            'id' => 8,
            'title' => 'Decorator',
            'description' => 'Upload 250 images',
            'criteria' => 250,
            'karma_points' => 125,
            'image' => 'extensions/Karma/images/Upload 250.webp',
        ],
        [
            'id' => 9,
            'title' => 'Art collector',
            'description' => 'Upload 500 images',
            'criteria' => 500,
            'karma_points' => 125,
            'image' => 'extensions/Karma/images/Upload 500.webp',
        ],
        [
            'id' => 10,
            'title' => 'Look at my collection!',
            'description' => 'Upload 750 images',
            'criteria' => 750,
            'karma_points' => 125,
            'image' => 'extensions/Karma/images/Upload 750.webp',
        ],
        [
            'id' => 11,
            'title' => 'I should start a museum or something',
            'description' => 'Upload 1000 images',
            'criteria' => 1000,
            'karma_points' => 125,
        ],
    ];
}
