<?php

class AchievementEarnedPresentationModel extends EchoEventPresentationModel {
    public function getIconType() {
        return 'karma-achievement-earned';
    }

    public function getHeaderMessage() {
        $achievementId = $this->event->getExtraParam('achievement-id');

        $dbw = wfGetDB(DB_MASTER);
        $achievement = $dbw->selectRow(
            'mw_achievements',
            '*',
            ['id' => $achievementId],
            __METHOD__
        );
        $achievementName = $achievement->title;

        return $this->msg('notification-header-achievement-earned', $achievementName);
    }


    public function getBodyMessage() {
        $karmaPoints = $this->event->getExtraParam('karma-points');
        return $this->msg('notification-body-achievement-earned', $karmaPoints);
    }

    public function getPrimaryLink() {
        return [
            'url' => SpecialPage::getTitleFor('Karma')->getFullURL(),
            'label' => $this->msg('karma-achievement-view')
        ];
    }
}
