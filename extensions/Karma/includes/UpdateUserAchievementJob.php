<?php

use MediaWiki\MediaWikiServices;

class UpdateUserAchievementJob extends Job {

    public function __construct( $title, $params, $id = 0 ) {
        parent::__construct( 'updateUserAchievement', $title, $params, $id );
    }

    public function run() {
        $title = $this->title;
        $userId = $this->params['userId'];
        $achievementTypes = $this->params['achievementTypes'];

        $dbw = wfGetDB(DB_MASTER);

        foreach ($achievementTypes as $achievementType) {
            $achievements = $dbw->select(
                'mw_achievements',
                '*',
                ['type' => $achievementType],
                __METHOD__
            );

            foreach ($achievements as $achievement) {
                $achievementId = $achievement->id;

                $currentProgress = $dbw->selectField(
                    'mw_user_achievement',
                    'progress',
                    [
                        'user_id' => $userId,
                        'achievement_id' => $achievementId
                    ],
                    __METHOD__
                );

                if ($currentProgress === false) {
                    $dbw->insert(
                        'mw_user_achievement',
                        [
                            'user_id' => $userId,
                            'achievement_id' => $achievementId,
                            'progress' => 1
                        ],
                        __METHOD__
                    );
                } else {
                    wfDebugLog('karma', 'Current progress: ' . $currentProgress . ', Progress required: ' . $achievement->progress_required);

                    if ($currentProgress <= $achievement->progress_required) {
                        $newProgress = $currentProgress + 1;
                        $completed = $newProgress >= $achievement->progress_required ? 'true' : 'false';

                        wfDebugLog('karma', 'Performing update for user: ' . $userId . ', achievement: ' . $achievementId . ', new progress: ' . $newProgress . ', completed: ' . $completed);

                        $dbw->update(
                            'mw_user_achievement',
                            [
                                'progress' => $newProgress,
                                'completed' => $completed
                            ],
                            [
                                'user_id' => $userId,
                                'achievement_id' => $achievementId
                            ],
                            __METHOD__
                        );

                        wfDebugLog('karma', 'Update performed.');

                        if ($completed == 'true') {
                            $timestamp = wfTimestamp(TS_ISO_8601, wfTimestampNow());

                            $dbw->insert(
                                'mw_user_karma',
                                [
                                    'user_id' => $userId,
                                    'achievement_id' => $achievementId,
                                    'points_earned' => $achievement->points,
                                    'earned_at' => $timestamp
                                ],
                                __METHOD__
                            );

                            try {
                                EchoEvent::create([
                                    'type' => 'karma-achievement-earned',
                                    'title' => $title,
                                    'extra' => [
                                        'achievement-id' => $achievementId,
                                        'karma-points' => $achievement->points,
                                        'notif-user-id' => $userId,
                                    ],
                                    'agent' => User::newFromId($userId),
                                ]);
                            } catch (Exception $e) {
                                wfDebugLog('karma', 'Error creating Echo event: ' . $e->getMessage());
                            }
                        }
                    } else {
                        wfDebugLog('karma', 'Update not executed because current progress is not less than progress required.');
                    }
                }
            }
        }
        return true;
    }
}
