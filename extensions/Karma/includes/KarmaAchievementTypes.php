<?php

class KarmaAchievementTypes {

    const EDIT = 'edit';
    const UPLOAD = 'upload';
    const PATROL = 'patrol';
    const EMAIL = 'email';
    const CATEGORY = 'category';
}
