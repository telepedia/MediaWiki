CREATE TABLE mw_user_karma (
  id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT NOT NULL REFERENCES user(user_id),
  achievement_id INT NOT NULL,
  points_earned INT NOT NULL,
  earned_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
