CREATE TABLE mw_achievements (
  id INT AUTO_INCREMENT PRIMARY KEY,
  title TEXT NOT NULL,
  description TEXT NOT NULL,
  image TEXT,
  points INT NOT NULL,
  type VARCHAR(50) NOT NULL,
  progress_required INT NOT NULL
);
