<?php

use MediaWiki\MediaWikiServices;
use MediaWiki\SpecialPage\SpecialPageFactory;

class KarmaHooks
{

    // bring in the SpecialPageFactory and initialize it
    public static function onExtensionLoad() {
        global $wgSpecialPageFactory;
        $wgSpecialPageFactory['Karma'] = [ self::class, 'createSpecialKarma' ];
    }

    public static function createSpecialKarma() {
        return new SpecialKarma(new KarmaService());
    }

    /**
     * call the job to do the actual incrementing in the database.
     * @param Title $title
     * @param $userId
     * @param array $achievementTypes
     * @return void
     */
    public static function enqueueUpdateUserAchievement(Title $title, $userId, array $achievementTypes) {
        $params = [
            'title' => $title->getPrefixedDBkey(),
            'userId' => $userId,
            'achievementTypes' => $achievementTypes,
        ];

        $job = new UpdateUserAchievementJob($title, $params);
        JobQueueGroup::singleton()->push($job);
    }


    /**
     * Send an increment to the job when a user saves an article.
     * @param WikiPage $wikiPage
     * @param \MediaWiki\User\UserIdentity $user
     * @param string $summary
     * @param int $flags
     * @param \MediaWiki\Revision\RevisionRecord $revisionRecord
     * @param \MediaWiki\Storage\EditResult $editResult
     * @return void
     **
     */
    public static function onPageSaveComplete(
        WikiPage                          $wikiPage,
        MediaWiki\User\UserIdentity       $user,
        string                            $summary,
        int                               $flags,
        MediaWiki\Revision\RevisionRecord $revisionRecord,
        MediaWiki\Storage\EditResult      $editResult
    )
    {
        wfDebugLog('karma', 'Edit hook triggered');
        self::enqueueUpdateUserAchievement($wikiPage->getTitle(), $user->getId(), [KarmaAchievementTypes::EDIT]);
    }

    /**
     * Send to the job queue when a user uploads a file.
     * @param $image
     * @return void
     **
     */
    public static function onUploadComplete(&$image)
    {
        wfDebugLog('karma', 'UploadComplete hook triggered');

        $mediawiki = MediaWikiServices::getInstance();
        $specialUpload = $mediawiki->getSpecialPageFactory()->getPage('Upload');
        $user = $specialUpload->getUser();
        $userId = $user->getId();
        $uploadedFile = $image->getLocalFile();
        $fileTitle = $uploadedFile->getTitle();

        self::enqueueUpdateUserAchievement($fileTitle, $userId, range(6, 11));
    }

    // set up our achievement
    public static function onBeforeCreateEchoEvent( &$notifs, &$categories, &$icons ) {
        $categories['achievement-earned'] = [
            'priority' => 9,
            'tooltip' => 'karma-pref-tooltip-achievement-earned',
        ];
        $notifs['karma-achievement-earned'] = [
            'category' => 'achievement-earned',
            'group' => 'positive',
            'section' => 'message',
            'canNotifyAgent' => true,
            'presentation-model' => AchievementEarnedPresentationModel::class,
            'bundle' => [
                'web' => true,
                'email' => true,
                'expandable' => true,
            ],
            EchoAttributeManager::ATTR_LOCATORS => [
                EchoUserLocator::class . '::locateEventAgent',
            ],
        ];
        $icons['karma-achievement-earned'] = [
            'path' => 'Karma/images/medal.svg',
        ];
    }

    // add the styles for the extension
    public static function onBeforePageDisplay(OutputPage &$out, Skin &$skin) {
        $out->addModules('ext.karma');
    }

    // set up the database when update.php is run
    public static function onLoadExtensionSchemaUpdates($updater) {
        $updater->addExtensionTable('mw_user_achievement', __DIR__ . '/sql/create_user_achievement.sql');
        $updater->addExtensionTable('mw_user_karma', __DIR__ . '/sql/create_user_karma.sql');
        $updater->addExtensionTable('mw_achievements', __DIR__ . '/sql/create_achievements_table.sql');

    }
}
