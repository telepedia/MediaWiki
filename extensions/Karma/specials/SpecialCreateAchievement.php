<?php

use MediaWiki\MediaWikiServices;
use MediaWiki\Permissions\PermissionManager;
use UploadBase;
use UploadStash;

class SpecialCreateAchievement extends FormSpecialPage
{
    public function __construct()
    {
        parent::__construct('CreateAchievement');
    }

    protected function getDisplayFormat()
    {
        return 'ooui';
    }

    protected function getFormFields()
    {
        wfDebugLog('karma', 'getFormFields Called');
        return [
            'title' => [
                'type' => 'text',
                'label-message' => 'createachievement-title',
                'required' => true
            ],
            'description' => [
                'type' => 'text',
                'label-message' => 'createachievement-description',
                'required' => true
            ],
            'image' => [
                'type' => 'file',
                'label-message' => 'createachievement-image',
                'required' => true,
                'accept' => [ '.png' ],
            ],
            'points' => [
                'type' => 'int',
                'label-message' => 'createachievement-points',
                'required' => true,
                'validation-callback' => [$this, 'validatePoints'],
            ],
            'type' => [
                'class' => 'HTMLSelectField',
                'label' => 'Achievement Type',
                'options' => [
                    'Edit' => KarmaAchievementTypes::EDIT,
                    'Upload' => KarmaAchievementTypes::UPLOAD,
                    'Patrol' => KarmaAchievementTypes::PATROL,
                    'Email' => KarmaAchievementTypes::EMAIL,
                    'Category' => KarmaAchievementTypes::CATEGORY,
                ],
            ],
            'progress_required' => [
                'type' => 'int',
                'label-message' => 'createachievement-progressrequired',
                'required' => true,
                'validation-callback' => [$this, 'validateProgressRequired'],
            ]
        ];
    }

    public function validatePoints($value)
    {
        return $value > 0 ? true : $this->msg('createachievement-points-invalid')->plain();
    }

    public function validateProgressRequired($value)
    {
        return $value > 0 ? true : $this->msg('createachievement-progressrequired-invalid')->plain();
    }


    public function onSubmit(array $data) {

        wfDebugLog('karma', 'onSubmit called with data: ' . print_r($data, true));


        $webRequestUpload = $request->getUpload( $data['image'] );
        $filename = $webRequestUpload->getName();
        $uploader = new UploadFromFile();
        $uploader->initialize( $filename, $webRequestUpload );

        $dbw = wfGetDB(DB_PRIMARY);

        $dbw->insert(
            'mw_achievements',
            [
                'title' => $data['title'],
                'description' => $data['description'],
                'image' => $fileName,
                'points' => $data['points'],
                'type' => $data['type'],
                'progress_required' => $data['progress_required']
            ],
            __METHOD__,
            ['IGNORE']
        );
        return Status::newGood();
    }
}

