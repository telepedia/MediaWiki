<?php

use MediaWiki\MediaWikiServices;

class SpecialKarma extends SpecialPage
{
    private $karmaService;

    function __construct(KarmaService $karmaService)
    {
        parent::__construct('Karma');
        $this->karmaService = $karmaService;
    }

    function execute($par)
    {
        $this->setHeaders();
        $out = $this->getOutput();
        $out->setPageTitle($this->msg('karma-title'));

        $user = $this->getUser();
        $userId = $user->getId();

        if ($userId === 0) {
            $out->addHTML('<p>You must be logged in to view your achievements.</p>');
            return;
        }

        $earnedAchievements = $this->karmaService->getEarnedAchievements($userId);
        $totalPoints = $this->karmaService->getTotalPoints($userId);

        $out->addHTML('<div class="karma-page">');

        $out->addHTML('<div class="karma-main">');
        if (empty($earnedAchievements)) {
            $out->addHTML('<p>You have not earned any achievements, keep going!</p>');
        } else {
            foreach ($earnedAchievements as $achievement) {
                $out->addHTML($this->achievementHTML($achievement, 'earned'));
            }
        }

        $unearnedAchievements = $this->karmaService->getUnearnedAchievements($userId);
        $out->addHTML('<h2>Keep going to earn these achievements:</h2>');
        foreach ($unearnedAchievements as $achievement) {
            $out->addHTML($this->achievementHTML($achievement, 'unearned'));
        }
        $out->addHTML('</div>'); // end karma-main

        $out->addHTML('<div class="karma-sidebar">');
        $out->addHTML('<div class="total-points">');
        $out->addHTML('<h2>Your Total Points</h2>');
        $out->addHTML('<p>' . $totalPoints . '</p>');
        $out->addHTML('</div>'); // end total-points

        $leaderboardData = $this->karmaService->getLeaderboardData();
        $out->addHTML('<div class="karma-leaderboard">');
        $out->addHTML('<h2>Leaderboard</h2>');
        $out->addHTML('<ul>');
        foreach ($leaderboardData as $data) {
            $leaderboardUser = User::newFromId($data['userId']);
            $out->addHTML('<li>' . htmlspecialchars($leaderboardUser->getName()) . ': ' . htmlspecialchars($data['totalPoints']) . ' points</li>');
        }
        $out->addHTML('</ul>');
        $out->addHTML('</div>'); // end karma-leaderboard

        $out->addHTML('</div>'); // end karma-sidebar
        $out->addHTML('</div>'); // end karma-page
    }

    function achievementHTML($achievement, $type) {
        $html = '<div class="achievement">';
        $html .= '<img src="'.$achievement['image'].'" alt="'.$achievement['title'].'" />';
        $html .= '<div class="achievement-details">';
        $html .= '<h2 class="KarmaTitle">'.$achievement['title'].'</h2>';
        $html .= '<p>'.$achievement['description'].'</p>';
        $html .= '</div>';
        $html .= '<div class="achievement-points">';
        if ($type === 'earned') {
            $html .= '<p>Points Earned: '.$achievement['points_earned'].'</p>';
            $html .= '<p>Earned At: '.$achievement['earned_at'].'</p>';
        } else {
            $html .= '<p>Karma Points: '.$achievement['points'].'</p>';
        }
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    protected function getGroupName()
    {
        return 'other';
    }
}
