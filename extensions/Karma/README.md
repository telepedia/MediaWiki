# Karma — An Achievement System

Karma is a MediaWiki extension that provides an achievement system for wiki's running on MediaWiki.
It is, as the name suggests, based on the Reddit "Karma" system. 

This extension has been developed by and for Telepedia, and as a result, might not be directly transferable to other wiki's/farms. 
Nonetheless, all effort has been made to make this extension as portable and configurable as possible, so it should work without any problems. 

## Installation

* Download and place the file(s) in a directory called Karma in your extensions/ folder.
* Register the extension in LocalSettings: `wfLoadExtension( 'Karma' );`
* Run `php update.php` to create the necessary tables required for the extension.
* Done - Navigate to `Special:Version` on your wiki to verify that the extension is successfully installed.

Please note that this extension absolutely requires a working job queue to function—you can either use ObjectCache, 
Redis, or some other caching of your choosing. This extension also requires Echo to send notifications when an achievement is earned.

## Configuration

## Security Concerns
If you believe that you have found a security vulnerability in this extension code, 
please do not post it publicly, or on-wiki/YouTrack. Please do not open an issue in this repository, 
doing so could potentially put users at risk. 

Instead, please send us an email with the security concern at community@telepedia.net, or contact Original Authority directly on Discord.
