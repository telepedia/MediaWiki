<?php

namespace MediaWiki\Extension\CloudflarePurge;

use Config;
use MediaWiki\Revision\RevisionRecord;
use MediaWiki\Storage\EditResult;
use Title;
use User;
use Cloudflare\API\Auth\APIKey;
use Cloudflare\API\Adapter\Guzzle;
use Cloudflare\API\Endpoints\Zones;

class HookHandler {

    /** @var Config */
    private $config;

    public function __construct( Config $config ) {
        $this->config = $config;
    }

    public static function onPageSaveComplete(
        $wikiPage, $user, $content, $summary, $isMinor, $isWatch, $section,
        $flags, RevisionRecord $revisionRecord, EditResult $editResult
    ) {
        $title = $wikiPage->getTitle();
        $url = $title->getFullURL();
        self::purgeCloudflare($url);
    }

    private static function purgeCloudflare($url) {
        $email = $this->config->get( 'CloudflareEmail' );
        $apiKey = $this->config->get( 'CloudflareAPIKey' );
        $zoneID = $this->config->get( 'CloudflareZoneID' );

        try {
            $key = new APIKey( $email, $apiKey );
            $adapter = new Guzzle( $key );
            $zone = new Zones( $adapter );
            $zone->cachePurge( $zoneID, [$url] );
        } catch (\Cloudflare\API\Endpoints\EndpointException $e) {
            // Cloudflare-specific exception
            wfDebugLog( 'CloudflarePurge', 'Cloudflare API EndpointException: ' . $e->getMessage() );
        } catch ( \Exception $e ) {
            // General exceptions
            wfDebugLog( 'CloudflarePurge', 'General exception: ' . $e->getMessage() );
        }
    }
}
