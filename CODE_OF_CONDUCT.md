Code of Conduct
===============

The development of this software is covered by the general MediaWiki [Code of Conduct](https://www.mediawiki.org/wiki/Special:MyLanguage/Code_of_Conduct). Furthermore, by contributing to the development of this software, you agree to be bound by the [Telepedia Community Charter](https://meta.telepedia.net/wiki/Telepedia_Community_Charter). 
